﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class global : MonoBehaviour
{

    public static double score;
    public static int deaths;
    public static int pickups;
    public static int polarity;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 300;
        score = 0;
        deaths = 0;
        pickups = 0;
        polarity = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
