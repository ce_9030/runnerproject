﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class globalCallback : MonoBehaviour
{

    public double callScore;
    public int callDeaths;
    public int callPickups;
    public int callPolarity;
    // Start is called before the first frame update
    void Start()
    {
        callScore = global.score;
        callDeaths = global.deaths;
        callPickups = global.pickups;
        callPolarity = global.polarity;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
