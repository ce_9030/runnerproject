﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scCamera : MonoBehaviour
{
    public int delaytostart = 500;
    public bool returntostart = false;
    public Transform targetBg;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Screen Shake
        if (delaytostart < 500 && delaytostart > 0 && returntostart == false) {delaytostart -= 1;
            transform.position += new Vector3(Random.Range(-0.005f, 0.005f), Random.Range(-0.005f, 0.005f), 0);
        }
        if (delaytostart == 0 && returntostart==false) { returntostart = true; delaytostart = -1;
            GetComponent<Camera>().orthographicSize = 0.998f;}
        if (transform.position.x > 0.1 && returntostart == true)
        { transform.position = new Vector3(transform.position.x, 0,-10);
            GetComponent<Rigidbody2D>().velocity = new Vector2(-10f, 0); }
        if (transform.position.x<0.1 && returntostart==true)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            transform.position = new Vector3(0, 0, -10);
            returntostart = false;
            delaytostart = 500;
        }

            targetBg.GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity / 1.05f;
    }
}
