using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scPartShrink : MonoBehaviour
{
    SpriteRenderer render;
    float spritesize;
    // Start is called before the first frame update
    void Start()
    {
        spritesize = 0.1f;
        render = GetComponent<SpriteRenderer>();
        render.color = new Color(render.color.r, render.color.g, render.color.b, Random.Range(0.4f, 0.8f));
    }

    // Update is called once per frame
    void Update()
    {
        spritesize -= 0.00025f;
        transform.localScale = new Vector3(spritesize, spritesize, 1);
        if (spritesize < 0.001) Destroy(gameObject);
    }
}
