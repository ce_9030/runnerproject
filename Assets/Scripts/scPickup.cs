using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scPickup : MonoBehaviour
{

    bool enable = true;
    AudioSource audioData;
    public Transform targetTransform;
    public Transform particle;
    public AudioClip getSound;
    SpriteRenderer render;
    
    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<SpriteRenderer>();
        audioData = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enable==false) render.color = new Color(render.color.r, render.color.g, render.color.b, 0f);
        else render.color = new Color(render.color.r, render.color.g, render.color.b, 1f);
        if (global.pickups == 0) enable = true;
        

        if ((transform.position - targetTransform.position).magnitude < 0.2f && enable==true)
        {
            audioData.PlayOneShot(getSound, 1f);
            for (int i = 0; i < 8; i++)
            {
                Transform getfx = Instantiate(particle, new Vector2(transform.position.x + Random.Range(-0.1f, 0.1f), transform.position.y + Random.Range(-0.1f, 0.1f)), transform.rotation) as Transform;
                getfx.GetComponent<Rigidbody2D>().velocity = transform.up * 0.3f;
                getfx.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0, 1f);
            }
            global.pickups++;
            enable = false;
        }
    }

}
