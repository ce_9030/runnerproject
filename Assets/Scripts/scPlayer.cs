﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scPlayer : MonoBehaviour
{
    Animator anim;
    SpriteRenderer render;
    AudioSource audioData;

    public Transform cameraTarget;
    public Transform goalTarget;
    public Transform squareParticle;
    public Transform resultTarget;
    public bool gameStart = false;
    public bool gameClear = false;
    public bool isDamaged = false;
    int damageConfirmation = 100;
    int damageDelay = 800;
    public double resultDelay = 1000;
    bool isRunning = false;

    public AudioClip sndJump1;
    public AudioClip sndJump2;
    public AudioClip sndJump3;
    public AudioClip sndHit;
    public AudioClip sndPolarity;
    public AudioClip sndWin;

    int isRunningAnim = Animator.StringToHash("isRunning");
    int isDamagedAnim = Animator.StringToHash("isDamaged");
    //float vspeedAnim = Animator.StringToHash("vspeed");

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        render = GetComponent<SpriteRenderer>();
        audioData = GetComponent<AudioSource>();
        global.polarity = 1;
    }

    void Update()
    {
        //Polarity
        if (Input.GetKeyDown("x"))
        {
            if (global.polarity == 1) global.polarity = 2; else global.polarity = 1;
            audioData.PlayOneShot(sndPolarity, 1f);
            for (int i=0;i<9;i++)
            {
                CreateParticle(0);
            }
        }
        if (global.polarity == 2) render.color = Color.blue;
        else
            GetComponent<SpriteRenderer>().color = Color.red;

        //Damage
        if ((GetComponent<Rigidbody2D>().velocity.x < 7.450581e-09 || transform.position.y < -1.25f
            || transform.position.x < cameraTarget.transform.position.x-1.6f) && gameStart == true && isDamaged == false && gameClear==false)
            damageConfirmation -= 1;
        else damageConfirmation = 100;
        if (damageConfirmation<1 && gameStart==true && isDamaged ==false)
        {
            isDamaged = true;
            anim.SetBool("isDamaged", true);
            cameraTarget.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            cameraTarget.GetComponent<scCamera>().delaytostart = 499;
            damageDelay = 799;
            audioData.PlayOneShot(sndHit, 1f);
            global.deaths++;
            global.pickups = 0;
        }

        //Reset to Start upon Damage
        if (damageDelay < 800 && isDamaged == true) damageDelay -= 1;
        if (damageDelay<1 && isDamaged==true) {
            anim.SetBool("isDamaged", false);
            anim.SetBool("isRunning", false);
            gameStart = false;
            isRunning = false;
            isDamaged = false;
            global.pickups = 0;
            damageDelay = 700;
            transform.position = new Vector2(-1.105f, -0.255f);
        }

        //Game Clear
        if (transform.position.x > goalTarget.position.x && resultDelay > 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0.5f, GetComponent<Rigidbody2D>().velocity.y);
            cameraTarget.GetComponent<Rigidbody2D>().velocity = new Vector2(0.5f, 0);
            anim.speed = 0.5f;
            if (resultDelay == 1000)
            {
                audioData.PlayOneShot(sndWin, 1f);
                for (int i = 0; i < 18; i++) CreateParticle(1);
            }
            gameClear = true;
        }

        if (resultDelay > 0 && gameClear==true) resultDelay-=0.25; else
            if (resultDelay < 1 && resultDelay != -101 && gameClear == true)
        {
            resultDelay = -101;
            anim.SetBool("isRunning", false);
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            cameraTarget.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            global.score += (5000+(global.pickups*200)-(global.deaths*250));
            if (global.score < 0) global.score = 0;
            resultTarget.GetComponent<scResultFade>().fade = 0.02f;
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //Jumping
        if (Input.GetKey("z") && !this.anim.GetCurrentAnimatorStateInfo(0).IsName("Jumping") && isDamaged == false && gameClear==false)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x - 0.3f, 1.5f);
            float randJump = Mathf.Round(Random.Range(1, 4));

            if (!cameraTarget.GetComponent<AudioSource>().isPlaying)
                switch (randJump)
            {
                case 1: cameraTarget.GetComponent<AudioSource>().PlayOneShot(sndJump1, 1f); break;
                case 2: cameraTarget.GetComponent<AudioSource>().PlayOneShot(sndJump2, 1f); break;
                case 3: cameraTarget.GetComponent<AudioSource>().PlayOneShot(sndJump3, 1f); break;
            }
        }
        anim.SetFloat("vspeed", GetComponent<Rigidbody2D>().velocity.y);


        //Running
        if (gameStart==false && Input.GetKey("z") && isDamaged==false && cameraTarget.transform.position.x<0.2)  {
            anim.SetTrigger(isRunningAnim);
            GetComponent<Rigidbody2D>().velocity = new Vector2(2, GetComponent<Rigidbody2D>().velocity.y);
            cameraTarget.GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x-0.08f, 0);
            isRunning = true;
            gameStart = true;
        }
        if (isRunning == true && gameClear==false && isDamaged==false)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(2, GetComponent<Rigidbody2D>().velocity.y);
            if (isDamaged==false) CreateParticle(0);
        }

    }

    public void CreateParticle(int mode)
    {
        if (mode == 0)
        {
            Transform polarityfx = Instantiate(squareParticle, new Vector2(transform.position.x + Random.Range(-0.1f, 0.1f), transform.position.y + Random.Range(-0.2f, 0.2f)), transform.rotation) as Transform;
            polarityfx.GetComponent<Rigidbody2D>().velocity = transform.right * -0.5f;
            if (global.polarity == 2) polarityfx.GetComponent<SpriteRenderer>().color = Color.blue;
            else polarityfx.GetComponent<SpriteRenderer>().color = Color.red;
        }

        else if (mode == 1)
        {
            Transform polarityfx = Instantiate(squareParticle, new Vector2(transform.position.x + Random.Range(-0.2f, 0.2f), transform.position.y + Random.Range(-0.4f, 0.4f)), transform.rotation) as Transform;
            polarityfx.GetComponent<Rigidbody2D>().velocity = transform.right * -0.5f;
        }
    }


}
