using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class scPolarityWall : MonoBehaviour
{
    public TilemapCollider2D col;
    public Tilemap render;
    public int polarityMode=1;
    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<TilemapCollider2D>();
        render = GetComponent<Tilemap>();
    }

    // Update is called once per frame
    void Update()
    {
        if (global.polarity != polarityMode) {
            render.color = new Color(render.color.r, render.color.g, render.color.b, 0.5f);
            col.enabled = false;}
        else {
            render.color = new Color(render.color.r, render.color.g, render.color.b, 1f);
            col.enabled = true;}
    }
}
