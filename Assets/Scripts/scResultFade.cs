using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scResultFade : MonoBehaviour
{
    SpriteRenderer render;
    Text text;
    public float fade = 0f;
    public Transform[] subsidiaries;
    public bool enableRetry = false;
    // Start is called before the first frame update
    void Awake()
    {
        if (enableRetry == true)
        {
            render = GetComponent<SpriteRenderer>();
            render.color = new Color(1f, 1f, 1f, 0f);
        }
        else
        {
            text = GetComponent<Text>();
            text.color = new Color(1f, 1f, 1f, 0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (fade<1f && fade>0f) fade += 0.02f;
        if (enableRetry == true)
        {
            render.color = new Color(1f, 1f, 1f, fade);
            for(int i = 0; i < subsidiaries.Length; i++)
                subsidiaries[i].GetComponent<scResultFade>().text.color = new Color(1f, 1f, 1f, fade);
        }

        if (enableRetry==true && Input.GetKey("z") && fade>0.9f) {
            SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
        }
    }
}
