using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scUIText : MonoBehaviour
{
    Text txt;
    public int mode = 0;
    //0: Score - 1: Pickups - 2: Deaths - 3: Pickup Tally - 4: Death Tally
    // Start is called before the first frame update
    void Start()
    {
        txt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        switch(mode)
        {
            case 0: txt.text = global.score.ToString(); break;
            case 1: txt.text = global.pickups.ToString()+"/10"; break;
            case 2: txt.text = global.deaths.ToString(); break;
            case 3: txt.text = global.pickups.ToString()+" x 200"; break;
            case 4: txt.text = global.deaths.ToString() + " x -250"; break;
        }
    }
}
