using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scZoomIn : MonoBehaviour
{
    public Transform cameraTarget;
    public Transform playerTarget;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((transform.position - playerTarget.position).magnitude < 0.6f && playerTarget.GetComponent<scPlayer>().isDamaged==false)
            cameraTarget.GetComponent<Camera>().orthographicSize -= 0.0002f;
    }
}
